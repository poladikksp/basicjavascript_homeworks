const tabs = document.querySelectorAll(".tabs-title");
const tabsContent = document.querySelectorAll(".tabs-content");
let tabName;
tabs.forEach((element) => {
  element.addEventListener("click", tabSelection);
});
function tabSelection() {
  tabs.forEach((elem) => elem.classList.remove("active"));
  this.classList.add("active");
  tabName = this.getAttribute("data-tab");
  tabsContent.forEach((elem) => {
    elem.classList.contains(tabName)
      ? elem.classList.add("active-content")
      : elem.classList.remove("active-content");
  });
}
