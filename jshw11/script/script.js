function showHidePassword(event) {
  if (event.target.tagName !== "I") return;
  const targetParent = event.target.parentNode;
  const openedEye = targetParent.querySelector(".fa-eye");
  const slashedEye = targetParent.querySelector(".fa-eye-slash");
  const input = targetParent.querySelector(".input");
  if (input.type === "password") {
    input.setAttribute("type", "text");
    slashedEye.classList.remove("disp-none");
    openedEye.classList.add("disp-none");
  } else {
    input.setAttribute("type", "password");
    slashedEye.classList.add("disp-none");
    openedEye.classList.remove("disp-none");
  }
}
function passwordCheck(event) {
  event.preventDefault();
  const firstInput = document.querySelector(".first-input");
  const secondInput = document.querySelector(".second-input");
  if (firstInput.value === "" || secondInput.value === "") {
    document.querySelector(".error").innerText = "Поля не должны быть пустыми!";
  } else {
    if (firstInput.value === secondInput.value) {
      document.querySelector(".error").innerText = "Добро пожаловать!";
    } else {
      document.querySelector(".error").innerText = "Пароли должны совпадать!";
    }
  }
}
const passwordForm = document.querySelector(".password-form");
passwordForm.addEventListener("click", showHidePassword);
passwordForm.addEventListener("submit", passwordCheck);
