// Чому для роботи з input не рекомендується використовувати клавіатуру?
// Тому що незважаючи на подію keydown якщо ми зробимо якийсь фільтр, або заборонимо натискання якихось клавіш то ми все одно зможемо ввести в інпут все що завгодно за допомогою миші, або вставки.

document.body.addEventListener("keydown", (event) => {
  const btns = document.querySelectorAll(".btn");
  btns.forEach((elem) => {
    elem.classList.remove("btn-active");
    if (elem.dataset.button.toLowerCase() === event.key.toLowerCase()) {
      elem.classList.add("btn-active");
    }
  });
});
