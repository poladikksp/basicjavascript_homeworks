// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval(`)`.
// setTimout запускає функцію після зазначенного часу, setInterval запускає функцію регулярно з зазначеним інтервалом.

// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
// При передачі нульової затримки функція спрацює одразу після завершення поточного скрипту тому вона не спрацює миттєво.

// 3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
// Для того щоб не займати зайву пам'ять браузера.

"use sctrict";

const images = document.querySelector(".images-wrapper").children;
const buttons = document.querySelector(".buttons");
let imgCount = 0;

function imgChanger(collection, btns) {
  btns.classList.add("buttons-show");

  const startTime = Date.now();
  let timer = setInterval(() => {
    const milliseconds = Date.now() - startTime;
    const secMillisec = 3000 - milliseconds;
    document.querySelector(".timer").innerText =
      secMillisec.toString().split("")[0] +
      "." +
      secMillisec.toString().slice(1) +
      " seconds to change";
    if (secMillisec <= 0) {
      document.querySelector(".timer").innerText = "Paused";
      clearInterval(timer);
    }
  }, 1);

  if (imgCount === collection.length) {
    imgCount = 0;
  }
  for (let i = 0; i < collection.length; i++) {
    if (imgCount === collection.length - 1) {
      collection[i].classList.remove("show");
      collection[0].classList.add("show");
    } else if (i === imgCount) {
      collection[i].classList.remove("show");
      collection[i + 1].classList.add("show");
    }
  }
  imgCount++;
}

let imgCycler = setInterval(imgChanger, 3000, images, buttons);

buttons.addEventListener("click", (event) => {
  if (event.target.type === "reset") {
    clearInterval(imgCycler);
    event.target.setAttribute("disabled", "true");
    event.target.nextElementSibling.removeAttribute("disabled");
  }
  if (event.target.type === "submit") {
    document.querySelector(".timer").innerText = "Starting...";
    imgCycler = setInterval(imgChanger, 3000, images, buttons);
    event.target.setAttribute("disabled", "true");
    event.target.previousElementSibling.removeAttribute("disabled");
  }
});
