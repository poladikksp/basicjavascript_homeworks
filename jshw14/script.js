if (localStorage.blue === undefined) {
  localStorage.setItem("blue", "0");
}
const styleSheet = document.createElement("link");
styleSheet.setAttribute("rel", "stylesheet");
styleSheet.setAttribute("href", "./css/blue-theme.css");
if (localStorage.blue === "1") {
  document.head.append(styleSheet);
}
function themeChange() {
  if (localStorage.blue === "0") {
    document.head.append(styleSheet);
    localStorage.blue = "1";
  } else if (localStorage.blue === "1") {
    document.head.lastElementChild.remove();
    localStorage.blue = "0";
  }
}
const themeChangeButton = document.querySelector(".theme-changer");
themeChangeButton.addEventListener("click", themeChange);
