// 1. Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
// Рекурсія - це коли функція в процесі виконання викликає саму себе. На прктиці астосовується для того щоб розбити одну дію на декілька малих дій. Функція виконує одну маленьку дію декілька разів завдяки рекурсії
"use strict";
let num = prompt("Enter the number for factorial calculation:");
while (Number.isNaN(+num)) {
  num = prompt("Enter the number for factorial calculation:", num);
}
num = +num;
function factorial(number, nextNumber) {
  if (number === 0) return number;
  if (number === 1 || nextNumber === 1) return number;
  num = num - 1;
  let result = number * num;
  return factorial(result, num);
}
document.querySelector("p").innerText = factorial(num);
