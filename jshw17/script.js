const student = {
  name: "",
  lastName: "",
  tabel: {},
};

student.name = prompt("Enter your name");
student.lastName = prompt("Enter your LastName");

let subject;
while (subject !== null) {
  subject = prompt("Enter subjects name");
  if (subject === null) continue;
  let mark = prompt("Enter the mark from this subject");
  if (subject !== "" && mark !== "") {
    student.tabel[`${subject}`] = +mark;
  }
}

let badMarkCount = 0;
let marksSumm = 0;
for (const key in student.tabel) {
  if (student.tabel[key] < 4) {
    badMarkCount++;
  }
  marksSumm += student.tabel[key];
}

const middleMark = marksSumm / Object.keys(student.tabel).length;
if (badMarkCount === 0) {
  alert("Студента переведено");
}
if (middleMark > 7) {
  alert("Студенту призначено стипендію");
}
console.log(student);
