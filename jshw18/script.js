"use strict";
const oldObject = {
  "null property": null,
  country: "Ukraine",
  city: "Kyiv",
  cities: ["kharkiv", "Kherson", "Odessa"],
  colors: {
    top: "blue",
    bottom: "yellow",
    "random object": {
      random: "%",
      something: "qwerty",
    },
  },
  num: [
    85,
    90,
    56,
    45,
    { name: "Polad", studying: { college: "dan-it" }, age: 23 },
    34,
    56,
    [100, 100],
    99,
  ],
};

function deepCopyObject(obj) {
  const newObject = {};
  for (const key in obj) {
    if (obj[key] === null) {
      newObject[key] = null;
    } else if (Array.isArray(obj[key])) {
      const array = [];
      obj[key].forEach((elem) => {
        if (typeof elem === "object" && elem !== null) {
          array.push(deepCopyObject(elem));
        } else array.push(elem);
      });
      newObject[key] = array;
    } else if (typeof obj[key] === "object") {
      newObject[key] = deepCopyObject(obj[key]);
    } else {
      newObject[key] = obj[key];
    }
  }
  return newObject;
}

console.log(oldObject);
console.log(deepCopyObject(oldObject));
