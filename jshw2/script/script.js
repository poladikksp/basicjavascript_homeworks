// 1. Які існують типи даних у Javascript?
// Рядок, число, логічний тип даних, об'єкти, undefined, NaN, null, bigint.

// 2. У чому різниця між == і ===?
// == - звичайна рівність, перетворює операнди у числа та порівнює.
// === - строга рівність, виконує порівняння без перетворення типу.

// 3. Що таке оператор?
// Оператор - це функція що вбудована в JavaScript. Використовуючи оператор ми запускаємо вбудовану функцію яка проводить операції та повертає результат.

"use strict";

let name = prompt("What is your name?");
let age = +prompt("How old are you?");

while (Boolean(name) == false || Boolean(age) == false) {
  name = prompt("What is your name?", `${name}`);
  age = +prompt("How old are you?", `${age}`);
}

if (age < 18) {
  alert("You are not allowed to visit this website!");
} else if (age >= 18 && age <= 22) {
  const continueOrNot = confirm("Are you sure you want to continue?");
  if (continueOrNot == true) {
    alert(`Welcome, ${name}!`);
  } else {
    alert("You are not allowed to visit this website!");
  }
} else {
  alert(`Welcome, ${name}!`);
}
