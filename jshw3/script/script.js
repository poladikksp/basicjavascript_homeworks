// 1. Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
// Цикли потрібні в тому випадку коли нам потрібно один і той же код виконати декілька разів. Наприклад виконувати якийсь код поки істинна якась умова, або навпаки поки умова не стане істинною.

// 2. Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
// Цикл for я використовував в тих випадках коли мені окрім запису умови виконання цикло потрібно було також ініціалізувати якісь змінні що потрібні для роботи циклу та виконувати ітерацію. Цикл while використовував в тих впиадках коли цикл мав лише умову виконання.

// 3. Що таке явне та неявне приведення (перетворення) типів даних у JS?
// Явне перетворення - це перетворення значення одного типу в інший тип за допомогою функцій перетворення типів: Number(), Boolean(), String(), toString().

// Неявне перетворення - це автоматичне перетворення значення одного типу в інший тип при тому що ми не використовуємо функції перетворення типів. Наприклад:
//let num1 = 2; //оголосимо змінну числового типу
//let num2 = "2"; //оголосимо змінну типу рядок
//console.log(num1 + num2, typeof(num1 + num2)); //як бачимо результат 22 string, тобто значення num1 автоматично перетворилося у рядок після чого рядки були об'єднані в один, відбулося неявне перетворення типу.

"use strict";

let userNumber = +prompt("Choose your number");
while (!(userNumber % 1 === 0)) {
  userNumber = +prompt("Choose your number");
}
for (let i = 0; i <= userNumber; i = i + 1) {
  if (userNumber < 5) {
    console.log("Sorry, no numbers :(");
    break;
  } else if (i % 5 === 0) {
    console.log(i);
  }
}

// код для другого пункту завдання підвищенної складності:

// let m = +prompt("Введіть перше число");
// let n = +prompt("Введіть друге число");
// while (!(m % 1 === 0) || !(n % 1 === 0)) {
//   m = +prompt("Введіть перше число");
//   n = +prompt("Введіть друге число");
// }
// if (m > n) {
//   let x = m;
//   let y = n;
//   m = y;
//   n = x;
// }
// isprime: for (let i = m; i <= n; i++) {
//   if (i === 0 || i === 1) {
//     continue isprime;
//   }
//   for (let j = 2; j < i; j++) {
//     if (i % j === 0) {
//       continue isprime;
//     }
//   }
//   console.log(i);
// }
