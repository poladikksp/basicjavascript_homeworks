// 1. Описати своїми словами навіщо потрібні функції у програмуванні.
// Функції потрібні у випадку коли потрібно якийсь шматок коду повторювати декілька разів. Функції дозвонляють уникнути дублювання коду, пишемо функцію один раз, а потім просто викликаємо її у потрібному місці.

// 2. Описати своїми словами, навіщо у функцію передавати аргумент.
//Аргументи потрібні для того щоб передавати дані у функцію. З цими данними і працює функція.

// 3. Що таке оператор return та як він працює всередині функції?
// return використовується для повернення результату виконання функції. У функції може бути необмежена кількість return всередині, але виконуватись буде тільки один, як тільки спрацює один з них функція завершить своє виконання та поверне результат. Якщо не вказати return, або написати його та не вказати його значення функція буде повертати undefined.
"use strict";
let num1 = prompt("Enter the first number");
let num2 = prompt("Enter the second number");
let mathAction = prompt("Enter a mathematical action");
while (num1 === "" || isNaN(+num1) || num2 === "" || isNaN(+num2)) {
  num1 = prompt("Enter the first number", `${num1}`);
  num2 = prompt("Enter the second number", `${num2}`);
  mathAction = prompt("Enter a mathematical action", `${mathAction}`);
}
while (
  !(mathAction === "+") &&
  !(mathAction === "-") &&
  !(mathAction === "*") &&
  !(mathAction === "/")
) {
  alert("Enter correct mathematical operation!!!(+, -, *, /)");
  mathAction = prompt("Enter a mathematical action", `${mathAction}`);
}
function mathResult(a, b, c) {
  if (c === "+") {
    return +a + +b;
  }
  if (c === "-") {
    return +a - +b;
  }
  if (c === "*") {
    return +a * +b;
  }
  if (c === "/") {
    return +a / +b;
  }
}
let result = mathResult(num1, num2, mathAction);

console.log(result);
