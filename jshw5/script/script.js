// 1. Опишіть своїми словами, що таке метод об'єкту.
// Метод об'єкту це функція що являється властивістю об'єкта яка працює з власивостями об'єкту та переданими до неї аргументами. Викликається точно также як і властивість. При виклику метода всередині об'єкта потрібно використовувати this.

// 2. Який тип даних може мати значення властивості об'єкта?
// Властивість об'єкта може мати будь-який тип даних.

// 3. Об'єкт це посилальний тип даних. Що означає це поняття?
// Це означає що при створенні об'єкту змінна не зберігає сам об'єкт, а посилання на нього в пам'яті комп'ютера.

"use strict";
function createNewUser() {
  const name = prompt("Enter your name");
  const lastName = prompt("Enter your last name");
  const newUser = {
    _firstName: name,
    _lastName: lastName,
    set firstName(name) {
      Object.defineProperty(newUser, "_firstName", { writable: true });
      this._firstName = name;
    },
    set lastName(lastname) {
      Object.defineProperty(newUser, "_lastName", { writable: true });
      this._lastName = lastname;
    },
    getLogin() {
      return (
        `${this._firstName}`[0].toLowerCase() +
        `${this._lastName}`.toLowerCase()
      );
    },
  };
  Object.defineProperty(newUser, "_firstName", { writable: false });
  Object.defineProperty(newUser, "_lastName", { writable: false });
  console.log(newUser.getLogin());
  return newUser;
}
console.log(createNewUser());
