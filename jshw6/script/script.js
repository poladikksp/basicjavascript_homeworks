// 1. Опишіть своїми словами, що таке екранування, і навіщо воно потрібне в мовах програмування
//Екранування використовується коли нам потрібно використовувати спецсимволи в середині рядка. 
// Використовують "Символ екранування" зворотній слєш "\" після чого записують спецсимвол

// 2. Які засоби оголошення функцій ви знаєте?
// Function declaration (оголошення функціі):
// function fn(){};
// Function expression (створення змінної в яку записується функція)
// const func = function(){}

// 3. Що таке hoisting, як він працює для змінних та функцій?
// hoisting це механізм при якому оголошення функцій та змінних переміщуються вверх своєї області значеть перед тім як виконається код.
// це означає що неважливо в якому місці буде оголошено функцію вона створиться перед тим як запуситься код, саме тому ми можемо викликати функцію
// перед її оголошенням. У випадку з змінними JS при першому проході кода буде знати про існування змінних вже до початку виконання коду. 
// змінній var за замовчуванням надаэться undefined. let та const створюються без значеть, значення надаються лише при виконанні коду.

"use strict";
function createNewUser() {
  const name = prompt("Enter your name");
  const lastName = prompt("Enter your last name");
  const bday = prompt("Enter your birthday in dd.mm.yyyy form");
  const newUser = {
    _firstName: name,
    _lastName: lastName,
    birthday: bday,
    set firstName(name) {
      Object.defineProperty(newUser, "_firstName", { writable: true, value: name });
    },
    set lastName(lastname) {
      Object.defineProperty(newUser, "_lastName", { writable: true, value: lastname });
    },
    getLogin() {
      return `Users login is: ${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`;
    },
    getAge() {
      const bdayFormatChange = this.birthday.split(".").reverse().join("-");
      const bdayDate = new Date(bdayFormatChange);
      const age = Date.now() - bdayDate.getTime();
      return `Users age is: ${parseInt(age / 1000 / 60 / 60 / 24 / 365)} years`;
    },
    getPassword() {
      return `Users password is: ${this._firstName[0].toUpperCase()}${this._lastName.toLowerCase()}${this.birthday.slice(6, 10)}`;
    },
  };
  Object.defineProperty(newUser, "_firstName", { writable: false });
  Object.defineProperty(newUser, "_lastName", { writable: false });
  return newUser;
}
let newUser = createNewUser();
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
console.log(newUser);