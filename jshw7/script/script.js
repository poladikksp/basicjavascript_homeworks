// 1. Опишіть своїми словами як працює метод forEach.
// метод працює за допомогою коллбек функції, перебирає кожен елемент массиву та застосовує до нього цю функцію.

// 2. Як очистити масив?
// задати довжині массиву значення 0. arr.length = 0.

// 3. Як можна перевірити, що та чи інша змінна є масивом?
// typeof масиву поверне об'єкт тому для перевірки на масив існує спеціальний метод Array.isArray(array).

"use strict";
function filterBy(array, dataType) {
  if (!Array.isArray(array)) {
    return "that thing that you entered... it's not an array!";
  } else if (
    ![
      "undefined",
      "boolean",
      "number",
      "string",
      "object",
      "symbol",
      "bigint",
      null,
    ].includes(dataType)
  ) {
    return `yep that's an array but there is no such a type like ${dataType}...`;
  }

  // найпростіший варіант:
  return array.filter((item) => typeof item !== dataType);

  // Варіант через метод forEach з перевіркою null:
  // let filteredArray = [];
  // array.forEach((item) => {
  //   if (dataType === null && item !== null) {
  //     filteredArray.push(item);
  //   } else if (dataType === "object") {
  //     if (typeof item !== dataType || item === null) {
  //       filteredArray.push(item);
  //     }
  //   } else if (
  //     dataType !== null &&
  //     typeof item !== dataType
  //   ) {
  //     filteredArray.push(item);
  //   }
  // });
  // return filteredArray;

  //Варіант через метод filter з перевіркою null:
  // if (dataType === null) {
  //   return array.filter((item) => item !== dataType);
  // } else if (dataType === "object") {
  //   return array.filter((item) => {
  //     if (item !== null) {
  //       return !(typeof item === dataType);
  //     } else {
  //       return true;
  //     }
  //   });
  // } else {
  //   return array.filter((item) => !(typeof item === dataType));
  // }
}
const modArray = filterBy(
  [
    undefined,
    undefined,
    true,
    false,
    25,
    64,
    21364444n,
    21456588n,
    Symbol(),
    Symbol(),
    "Java",
    "Script",
    null,
    null,
    {},
    {},
    [],
    [],
  ],
  "object"
);
console.log(modArray);
