// 1. Опишіть своїми словами що таке Document Object Model (DOM)
//Об'єктна модель документа - представляє HTML документ у вигляді об'єкту JS що дозволяє нам взаємодіяти з елементами та контентом через JavaScript.

// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//innerHTML повертає вміст вузла такий який він є, тобто з усіма тегами і тд. При присвоєнні можна вставляти HTML код.
//innerText повертає тільки текстовий вміст вузла, теги ігноруються. При присвоєнні HTML теги буде вставлено як текст.

// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// Звернутись до елемента можна за допомогою методів:
// querySelector
// querySelectorAll
// getElementById
// getElementsByName
// getElementsByTagName
// getElementsByClassName
// Найкращі способи це querySelector та querySelectorAll вони найбільш універсальні та зручні.

"use strict";
// 1)
const pTags = document.getElementsByTagName("p");
for (const elem of pTags) {
  elem.classList.add("another-background");
}
// 2 спосіб
// const pTags = [...document.getElementsByTagName("p")];
// pTags.forEach(function (item) {
//   item.classList.add("another-background");
// });

// 2)
const getById = document.getElementById("optionsList");
console.log(getById);

const getByIdParent = getById.parentElement;
console.log(getByIdParent);

if (getById.hasChildNodes()) {
  const getByIdChilds = getById.childNodes;
  for (const node of getByIdChilds) {
    console.log(node.nodeName + " " + node.nodeType);
  }
}

// 3)
const testParagraph = document.getElementById("testParagraph");
testParagraph.innerText = "This is a paragraph";

// 4)
const allLi = document.querySelectorAll(".main-header > div > ul > li");
for (const elem of allLi) {
  elem.classList.add("nav-item");
}
console.log(allLi);

// 5)
const sectionTitle = document.querySelectorAll(".section-title");
for (const elem of sectionTitle) {
  elem.classList.remove("section-title");
}
console.log(sectionTitle);
