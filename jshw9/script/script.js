// 1. Опишіть, як можна створити новий HTML тег на сторінці.
// Новий HTML тег можна створити за допомогою метода documen.createElement(tag);

// 2. Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// elem.insertAdjacentHTML(where, html) перший параметр вказує куди відносно elem потрібно вставляти html.
// beforebegin - вставить перед elem
// afterbegin - вставить в початок elem
// beforeend - вставить в кінець elem
// afterend - вставить після elem

// 3. Як можна видалити елемент зі сторінки?
// За допомогою метода elem.remove();

const array = [
  "hello",
  ["array", "in", "array", ["array", "in", "array", "in", "array", ["Uman"]]],
  "world",
  "Kiev",
  "Kharkiv",
  ["array", "in", "array", ["array", "in", "array", "in", "array"]],
  "Odessa",
  "Lviv",
  "Kherson",
];
const element = document.querySelector(".test-element");

function listCreator(arr, elem) {
  const newList = document.createElement("ul");
  newList.classList.add("remove");
  let newListItem;
  arr.forEach((element) => {
    if (Array.isArray(element)) {
      listCreator(element, newList);
    } else {
      newListItem = document.createElement("li");
      newListItem.innerText = element;
      newList.append(newListItem);
    }
  });
  elem === undefined ? document.body.append(newList) : elem.append(newList);
}

let timer = document.createElement("p");
timer.classList.add("timer");
document.body.after(timer);
let i = 3;
let seconds = setInterval(() => {
  timer.innerText = "Deleting in " + i--;
}, 1000);
setTimeout(() => {
  document.querySelector(".remove").remove();
  timer.innerText = "Deleted successfully";
  clearInterval(seconds);
}, 4000);

listCreator(array, element);
